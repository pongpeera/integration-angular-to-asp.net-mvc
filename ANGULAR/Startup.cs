﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ANGULAR.Startup))]
namespace ANGULAR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
